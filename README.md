# Spectrum Monitor

*This is the public repository for the Spectrum Monitor project. All code and documents available here are viewable by the public with permission from Clearbox Systems.*
*The project is under an [NDA/IP agreement](https://drive.google.com/open?id=1DJHGzpYkIPh8KBGyznGqwTbXjb2dw9sW) that restricts public access to code and documentation. Therefore, no source code or detailed documentation will be made available.*

## Project Audits
[Audit 1 Slides](https://docs.google.com/presentation/d/1j1LoHBz-feoXFMkkTu0VtezJ-UNIX_-RprW760171zw/edit?usp=sharing)
<br>[Audit 1 Review Action Plan](https://docs.google.com/document/d/1vZfDXgo61l-LoQ_ErkZPrQwW2yf--Zc7yp5-59pZ6ZQ/edit?usp=sharing)<br>
[Audit 2 Slides](https://docs.google.com/presentation/d/1zl0VWuzNdyi7E4pVzupXP7MvpXqi9vWGRLfM2eDXqk4/edit?usp=sharing)<br>
[Audit 2 Review Action Plan](https://drive.google.com/open?id=1iR6ufinJdWfSWgfbqeEzpHIvBhf2pwqnhL62iInyc3c)<br>
[Audit 3 Slides](https://docs.google.com/presentation/d/1MmZ55Okn5TosIjB6FOWAbCRZwIRkIQjcFlMThRieVks/edit?usp=sharing)

## Documentation and Artefacts
All documentation throughout the project will be stored in our [Google Drive Folder](https://drive.google.com/drive/folders/1ymzxkfniGkgM8kXargKzbOD2qzmYXfI3?usp=sharing).

### Quick Directory Links
* General documentation
    * [Software Requirements Specification](https://drive.google.com/open?id=1KFjif300PVmg8YOhQwt8cUqB2DF235bQyMAPM7ZIjw0)   
    * [Test Cases](https://drive.google.com/open?id=1VYdiaZ3nYOmJYkwlwaxLMdaC0JCDIP4XMPJNqVCEpWY)
    * [Requirements Traceability Matrix](https://drive.google.com/open?id=1P6jwqVfPx0npl7Oic7XgjsQEUTf3bVsVr8dRMAnLgkI)
    * [Review Action Plan 1](https://drive.google.com/open?id=1vZfDXgo61l-LoQ_ErkZPrQwW2yf--Zc7yp5-59pZ6ZQ)
    * [Review Action Plan 2](https://drive.google.com/open?id=1iR6ufinJdWfSWgfbqeEzpHIvBhf2pwqnhL62iInyc3c)
* [Demo Recordings](https://drive.google.com/open?id=1pBz5Ypg5hvkh87c1xdRjSHPQPiAQsvn4)
* [Client meetings](https://drive.google.com/open?id=1bNKiHCKEiq4Oyv2UG_7Iu12Qv4yfr44N)
* [Team meetings](https://drive.google.com/open?id=1Q3eVhvcIVYIQ2zbci-1eNyciUvH-x_Gz)
* [Tutor meetings](https://drive.google.com/open?id=1XDqN6j1kPhHUIezSHZP1mlpaBTz2fvRU)
* [Decision log](https://gitlab.cecs.anu.edu.au/u6050672/Spectrum_Monitor/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=Type%3A%20Decision)
* [Communication log](https://drive.google.com/drive/folders/16uAbH8_QoxjYB8Q7gS7J0998PWMEUCjC?usp=sharing)
* [OBS](https://drive.google.com/open?id=1pJyWrIovYT1MFYSCYx-ahQgDIgXrCsbW)
* [NDA/IP agreement](https://drive.google.com/open?id=1DJHGzpYkIPh8KBGyznGqwTbXjb2dw9sW)

### Decision Making
Issue tracking and task assignment is available [here] (https://gitlab.cecs.anu.edu.au/u6050672/Spectrum_Monitor/issues). GitLab issues are used to track decisions made throughout the project. These issues are categorised with a "decision" label and can be found [here](https://gitlab.cecs.anu.edu.au/u6050672/Spectrum_Monitor/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Type%3A%20Decision). Feedback will also be recorded as [_Type: Suggestion_](https://gitlab.cecs.anu.edu.au/u6050672/Spectrum_Monitor/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Type%3A%20Suggestion) issues.

*Note: these issues can be sorted by date, priority (etc.) using the base GitLab controls on the right-hand side of the issue search bar.*

## Project Overview
### What
The project aims to complete the integration of two components into Clearbox Systems' "Foresight" software: a SED "decimator" device and a radio frequency (RF) spectrum monitoring display. These components will be standalone modules for use by Clearbox Systems' integration engineers and satellite network operators in the Australian Defence sector, and potentially Government and Commercial sectors in the future. 

The main goals of the project includes implementing a driver (written in Java) for a SED decimator to allow communication between the physical device and Foresight. Data read and pushed to Foresight must then be displayed on various graphs through HTML5 and TypeScript. In addition, simulated data will be required for demonstration capabilities and testing throughout the project.

### Why
Radio-based technologies and development has become commonplace in society (National Aeronautics and Space Administration, 2012). Electromagnetic spectrum is now used for a multitude of wireless communication, which requires heavy regulation to prevent harmful interferences. In particular, the Australian Defence Force requires consistent and continual radiocommunications for strategic, tactical and administrative functions (Australian Communications and Media Authority, 2017). 

The RF spectrum monitoring software will provide a simplified interface for visually detecting anomalies and convenience for monitoring radio signals. Alternative uses of RF spectrum monitoring includes meteorological purposes (radars, precipitation analysis etc.) and radio astronomy. Therefore, this project will enable further expansion of spectrum monitoring in the Defence sector and potentially Government and Commercial sectors.

### When
The project will begin from 22^nd February 2018, with completion of main goals expected by 25^th May 2018. A call for the project to continue in the second semester will be available if required.

#### Milestones
| Due Date (end of week) | Name | Description | Status |
| --- | --- | --- | --- |
| Week 3 | Requirements gathering | Draft software requirements document & requirements traceability matrix | Completed |
| Week 5 | Requirements/work breakdown/schedule/package prototypes | Finalise documentation, test methods for spectrum display | Completed |
| Week 6 | Spectrum graph prototypes | Prototypes of 2D and spectogram graphs | Completed |
| Week 7 | Decimator prototypes | Prototypes for the decimator driver with unit tests | Completed |
| Week 9 | Decimator driver | Complete implementation of the decimator driver with unit tests | On hold |
| Week 9 | Spectrum display through HTML5 | Both 2D and spectogram graphs displayed with controls for changing axes (etc.) | Completed |
| Week 10 | Spectrum display and decimator driver in Foresight | Development branch merged with master branch, driver working with Foresight | On hold |
| Week 12| Documentation | Application manuals and all project/testing artefacts | On hold |
| Week 12| Simulation suite | More realistic simulation of data for both drivers and spectrum display | On hold |

See the [Requirements Traceability Matrix](https://docs.google.com/spreadsheets/d/1P6jwqVfPx0npl7Oic7XgjsQEUTf3bVsVr8dRMAnLgkI/edit?usp=sharing) for more information on progress of sub tasks and requirements.

### Where
Integration of components and development of software will mainly take place at Clearbox Systems Canberra Facility (Australia).

### How
The Australian National University TechLauncher team for the project, consisting of engineering and computer science students, will work in close collaboration with Clearbox Systems to deliver the RF spectrum monitoring software.

## Client
[Clearbox Systems](http://www.clearboxsystems.com.au/ "Official Website") is an Australian, leading supplier and integrator of multiple products for Communications Network Management, Spectrum Management, Sensor Integration and Monitoring and Control systems. They support a wide range of customers including Defence, Government and Commercial sectors, and provide solutions and expertise both domestically and internationally in Satellite Communications, TV and Radio Broadcast Networks, RF and Digital Signal Processing. Currently, several of Clearbox Systems' customers includes the Australian Defence Force, the Capability Acquisition and Sustainment Group from the Department of Defence, Telstra, and the Australian Broadcasting Corporation.

Key members involved with the project will be Tim Spitzer and Jeremy Hallett.

*Note: All information provided on Clearbox Systems was adapted from their publicly available website at: http://www.clearboxsystems.com.au/company/about/.*


## Team
| Role | Name | Responsibilities | Degree |
| --- | --- | --- | --- | ... |
| Front-end Developer | Patrick Benter | Spectrum display design, implementation and testing | BSE (Software Engineering) |
| Back-end Developer | Richard Belihomji | Meeting organiser, minutes recorder, decimator driver implementation/integration | BEng/IT (Engineering/Information Technology) |
| Project Manager | Thien Bui-Nguyen | Documentation management, issue tracking, spectrum display implementation | BAC (Computer Science) |
| Back-end Developer | Hanzhong Cao | Decimator driver implementation | BE (Electrical Engineering) |
| Technical Lead | Kuangda He | Spectrum display implementation, database interaction, decimator driver implementation/integration | BE (Mechatronic Systems) + BSC (Computer Science) |

For more information, see our [Organisational Breakdown Structure](https://drive.google.com/open?id=1pJyWrIovYT1MFYSCYx-ahQgDIgXrCsbW).

### Communication
[Slack](https://spectrum-techlauncher.slack.com/) (Private) will be used for private communication between team members and the client.
<br>
[Facebook](https://www.facebook.com/) will be used for online group calls. 
<br>A [communication log](https://drive.google.com/open?id=16uAbH8_QoxjYB8Q7gS7J0998PWMEUCjC) from our general Slack chat is shared publicly (with restricted access to links).

## References
Australian Communications and Media Authority, 2017, Australian Radiofrequency Spectrum Plan 2017, viewed 3 March 2018,<br> https://www.acma.gov.au/~/media/Spectrum%20Engineering/Information/pdf/ARSP%202017%20-%20with%20general%20information%20pdf.pdf 

Clearbox Systems, n.d., [Official website of Clearbox Systems], viewed 3 March 2018,<br>
http://www.clearboxsystems.com.au/

National Aeronautics and Space Administration, Electromagnetic Spectrum Regulation, 2012, viewed 3 March 2018,<br>
https://www.nasa.gov/directorates/heo/scan/spectrum/txt_accordion3.html 

