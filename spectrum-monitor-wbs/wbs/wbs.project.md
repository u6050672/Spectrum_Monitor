# Spectrum Monitor Project WBS

## Requirements

chart {#stories-chart}

toggle {#stories-toggle}

### Research

- **Requirements gathering**: --- {story=research-req group="Research Phase"}
- **Prototyping and Testing**: --- {story=research-test group="Research Phase"}

totals {#stories-total group="Research"}

### Functional

- **Spectrum Display**: Devices have a “spectrum display” option in Foresight for opening the relevant HTML page {story=FR1 group="Functional"}
- **Spectrum Display**: Spectrum display page opens after a user clicks the spectrum display option {story=FR2 group="Functional"}
- **Spectrum Display**: Both 2D and waterfall graphs displayed simultaneously when the page loads {story=FR3 group="Functional"}
- **Spectrum Display**: RF Spectrum data is retrieved from the database when the HTML page loads {story=FR4 group="Functional"}
- **Spectrum Display**: RF spectrum data is plotted on 2D and waterfall graphs {story=FR5 group="Functional"}
- **Spectrum Display**: X and Y axis for graphs are displayed with labels {story=FR6 group="Functional"}
- **Spectrum Display**: Controls available to allow customisation of the Y-axis range for 2D graph {story=FR7 group="Functional"}
- **Spectrum Display**: A data simulation option is available that generates data and displays it on the 2D and waterfall graphs in the HTML page {story=FR8 group="Functional"}
- **Decimator Driver**: Data is sent from the decimator device to the driver {story=FR9 group="Functional"}
- **Decimator Driver**: All commands for the decimator implemented in device driver {story=FR10 group="Functional"}
- **Decimator Driver**: All statues for the decimator implemented in device driver {story=FR11 group="Functional"}
- **Paradise Modem Driver**: Data is sent from Paradise P310 modem to its driver {story=FR12 group="Functional"}
- **Paradise Modem Driver**: All commands for the Paradise P310 modem implemented in device driver {story=FR13 group="Functional"}
- **Paradise Modem Driver**: All statuses for the Paradise P310 modem implemented in device driver {story=FR14 group="Functional"}
- **Stretch Goal**: Other spectrum source used {story=FR15 group="Functional"}
- **Stretch Goal**: Perform signal characterisation on specrum data {story=FR16 group="Functional"}
- **Stretch Goal**: Perform signal classification on specrum data {story=FR17 group="Functional"}
- **Stretch Goal**: Perform signal tracking on specrum data {story=FR18 group="Functional"}
- **Stretch Goal**: Playback historical data {story=FR19 group="Functional"}

totals {#stories-total group="Functional"}

## Usability
- Customisation of which area to display on graphs is available {story=UR1 group="Usability"}
- Customisable contrast colours for the waterfall graph {story=UR2 group="Usability"}

totals {#stories-total group="Usability"}

## Reliability
- A maximum of 1Gb browser memory is in usage by the spectrum display at any time {story=RR1 group="Reliability"}


totals {#stories-total group="Reliability"}

## Performance
- The spectrum display page takes 2 seconds maximum to retrieve data from database and load graphs {story=PR1 group="Performance"}
- The spectrum display renders (at minimum) 5 times per second for data downloaded from the database {story=PR2 group="Performance"}

totals {#stories-total group="Performance"}

## Supportability
- HTML5 canvas designed for Firefox version 52.3.0 {story=SR1 group="Supportability"}
- Compatibility of spectrum display on other major web browsers such as Google Chrome and Safari {story=SR2 group="Supportability"}
- Graphs designed for 720p and 1080p resolutions will all functionality in terms of customisation visible/available {story=SR3 group="Supportability"}
- All code and technology used for the spectrum display open source {story=SR4 group="Supportability"}
- Documentation for the driver of the decimator device {story=SR5 group="Supportability"}
- Documentation for the driver of the Paradise P310 modem {story=SR6 group="Supportability"}

totals {#stories-total group="Supportability"}

### Total
totals {#stories-total}


## Spectrum Monitor Project
- 1 Research
  - [x] 1.1 [Requirements Document](https://docs.google.com/document/d/1KFjif300PVmg8YOhQwt8cUqB2DF235bQyMAPM7ZIjw0/edit#heading=h.tl3oobyz7hpf) {work=20 link=research-req}
  - [x] 1.2 Work Breakdown {work=2 link=research-req}
  - [ ] 1.3 D3 Performance Tests {work=2 link=PR1}
- 2 Spectrum Display
  - 2.1 Data Model
    - [ ] 2.1.1 Display Model {work=20 link=FR3}
    - [ ] 2.1.2 Data types {work=1 link=FR4}
    - [ ] 2.1.3 Data simulator {work=4 link=FR8} 
  - 2.2 Spectrum Views
    - 2.2.1 Full view
      - [ ] 2.2.1.1 Real-time view {work=10 link=FR3}
      - [ ] 2.2.1.2 Waterfall view {work=10 link=FR3}
    - [ ] 2.2.2 Widget view {work=4 link=FR1}
    - [ ] 2.2.3 UI Controls {work=4 link=FR7}
  - 2.3 Controller
    - [ ] 2.3.1 Display controller {work=8 link=FR1}
    - [ ] 2.3.2 UI Controls controller {work=8 link=FR7}
- 3 Device Driver
  - 3.1 Utility
    - [ ] 3.1.1 Parsing {work=2 link=FR9 link=FR10}
    - [ ] 3.1.2 Framing {work=2 link=FR1}
  - 3.2 Decimator IO
    - [ ] 3.2.1 Read {work=8 link=FR11}
    - [ ] 3.2.2 Write {work=2 link=FR10 confidence=30}
  - 3.3 Paradise P310 IO
    - [ ] 3.3.1 Read {work=8 link=FR14}
    - [ ] 3.3.2 Write {work=2 link=FR13 confidence=30}
- 4 System Testing
  - [ ] 4.1 Driver testing {work=2 link=PR2 confidence=30}
  - [ ] 4.2 Display testing {work=2 link=PR1 confidence=30}
- 5 Stretch Goals
  - [ ] 5.1 Additional Driver {work=8 link=FR15 confidence=30}
  - [ ] 5.2 Characterisation UI {work=20 link=FR16 confidence=30}
  - [ ] 5.3 Classification UI {work=20 link=FR17 confidence=30}
  - [ ] 5.4 Tracking UI {work=40 link=FR18 confidence=30}
  - [ ] 5.5 Playback {work=40 link=FR19 confidence=30}


## Administrative

- Stuff


## Raw Table Data

table {#stories-table}
